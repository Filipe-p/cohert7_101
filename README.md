# Docker 101

What is docker - docker is a container tool that help us build containers. 


## Pre Requisits 

- install docker
- have it working on the CLI 


## Main commands

### Running first container, Killing container & Port management
Running our first container 

```bash
docker run -dp 80:80 docker/getting-started

#docker run [-options] owner_repo/image
    # -d detached / deamon (run in background)
    # -p port specify a mapping of a port (80:80 host:gest)
        # The host id the phisical machine you are running docker
        # the guest is the app inside the container 
docker ps
```

Let's change the port to 7900

```
docker run -dp 7900:80 docker/getting-started
```

Now let's get all our runnig dockers, and kill the one on port 80

```
docker ps
--> CONTAINER ID (...)
--> 86d9cfeca9e9
--> 0bfde980110a

docker kill 0bfde980110a

docker ps 
--> CONTAINER ID (...)
--> 86d9cfeca9e9
```

#### Docker Hub!

https://hub.docker.com/

todo:

- What is docker hub?
  - Host images for docker container. 
- find the hello-word docker image
  - docker pull hello-world
  - docker run hello-world



### Nameing and Removing Containers

```
docker run --name my_appache httpd
```

--> Names of containers (docker_container + image) must be unique

Now we need to look for the container of that name and remove it because we want to run it in the background. 

```
# to list all containers (even not running)
docker ps -a

# remove container you want to remove using a name

docker rm my_apache

```

### Run with name and detached and getting information to terminal 

Debugging mode with output

run container with `-dit`

Where `d` detached
where `t` terminal 

--> creates logs

#### Some possible log location on linux:

- var/log/messages
- var/log/syslog
- var/lib/docker/containers/[id_of_containers]

`docker run -dit --name my_apache httpd`

`docker run -dit -p 80:80 --name my_apache httpd`

let's run another server on port 80.

```bash 
docker run -dit -p 80:80 --name nginx_proxy nginx
(...) Bind for 0.0.0.0:80 failed: port is already allocated.
```

so let's alocate a different port

```bash 
docker run -dit -p 82:80 --name nginx_proxy nginx
(...) Bind for 0.0.0.0:80 failed: port is already allocated.
```

#### Let's try install mysql or postgress

```
docker run mysql
--> fails
-->    - MYSQL_ROOT_PASSWORD
-->    - MYSQL_ALLOW_EMPTY_PASSWORD
-->    - MYSQL_RANDOM_ROOT_PASSWORD
```

on postgress
```
docker run postgress
--> fails
-->    - specify POSTGRES_PASSWORD to a non-empty value for the
       superuser
-->    - For example, "-e POSTGRES_PASSWORD=password" on "docker run"
```

**Passing in variable to docker run**

To pass variable use a key=value and the option -e 

```
docker run -e POSTGRES_PASSWORD=password POSTGRES_HOST_AUTH_METHOD=trust postgress
```

### Stoping, killing, starting and removing 

- `docker kill <container_id or coainer_name>`
- `docker stop <container_id or coainer_name>`
- `docker start <container_id or coainer_name>`


### using docker ps -q 

`-q` is the `--quiet` which `Only display container IDs`

Kill all containers:
- docker kill $(docker ps -q)

Remove all containers 
- docker kill $(docker ps -a -q)



### back to postress:

`docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgress -d postgres:latest`

- `-e VARIABLE=value` allows for variable input 
- `--name my_postgress -d` sets a name and allows for dettached running
- `postgres:latest` allows for specific versions


### Run some command indide the container

to run insde the container you'll use `exec`. There are some option. to keep the terminal open you pass -ti. 

The following is the syntar: 

`docker exec [OPTIONS] CONTAINER COMMAND`

`docker`

`docker exec -it my_postgres psql -U postgres -W`

```
CREATE TABLE BOOKS (Title VARCHAR(64),  
Author VARCHAR(300))
;
```

```
 \dt

 ```

 ```
$postgres=# exit
$docker stop my_postgress
--> my_postgress
$docker rm my_postgress
my_postgress
$ docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgress -d postgres:latest
-->c81210b5367777c483477a022dd0b37bc5c5bfec99dec71135fe4b65f36e627b
$ docker exec -it my_postgress psql -U postgres -W                                                                  
-->Password: 
-->psql (13.3 (Debian 13.3-1.pgdg100+1))
-->Type "help" for help.

$postgres=# \dt
-->Did not find any relations.

 ```

 **We lost all our data! :sadface:**


### INTRODUCING Docker Volumes

 Here to make your data percistant! 

 They can be shared between containers! WOOWOWOW!! **[CROWED GOES WILD!]** :taco: 


You put your emutable code in the container, and the mutable part in a attached volume. 

**use the -v**

Specicy the location in your `host:guest`
```
#syntax of -v
-v "path/local_dir:path/container_dir"
```

Example:
```
docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgress -d -v "$PWD/volumes_postgress:/var/lib/postgress" postgres:latest 

# Then creat a file in $PWD/volumes_postgress
touch ./volumes_postgress/hello.cat
echo "miau" >> ./volumes_postgress/hello.cat

# kill and rm the image
# see file is still there
```
### Docker volume commands 

these are some docker volume commands 

- docker volume [ls, rm, prune]


## Docker File 

It allows us to create our own containers from base containers. 

What type of OS is running in our out of the box docker httpd? 
- > try running an exec command with a package manager
- > did not know it what yum was, did know what apt is
- > means it's not a true httpd server, probably a appache2


**Let's write a true httpd server from a centos machine and change our amazing html welcome page**


### Docker File Commands

These are what we used
- FROM
- COPY
- RUN
- EXPOSE
- CMD & ENTRYPOINT 
  
**EXPOSE** Allows you to give metadata with regards to where your application/sw is running on what port. It's not the actual exposure of the port, that is done by the software / program in this case httpd. What is does is signal that we have something running on port 80 or what ever. 


### Builing an image 

`docker build -t <name> <directory>`


then you can run 

`docker run -dip --name -p 80:80 example_name new_image_name`

If you have sey your EXPOSE ports / metadata on which ports exist - Then you can run with capital `-P` to have docker automatically pickup the container's port 80 and map something to it. 

`docker run -dip --name -P example_name new_image_name`



## Docker Compose

Running two or more containers inside the service.
Compose files are in YAML.

main command:

- `docker compose up`
- `docker compose down`

docker compose example:

```
#version is what version of compose (not docker)
version: "3.9"
#services is where you specify whay you'll build
services:
  web:
    #build: . means build the docker file in this repository
    build: .
    # specifying ports
    ports:
      - "5000:5000"
  redis:
    # getting standard image from docker hub
    image: "redis:alpine"
```

other important things includ: 

- volumes:
- you can reference services directly 


## Docker Registry: 

Docker registry a service that stores and distributes images. 
Docker resgisty can have images identified with tags. 
You can then pull these images from anaywhere in your networkd. 

This service compares to Docker Hub - but locally. 

```

# Start your registry
docker run -d -p 5000:5000 --name registry registry:2

#Pull (or build) some image from the hub
docker pull ubuntu

#Tag the image so that it points to your registry
docker image tag ubuntu localhost:5000/myfirstimage

#Push it
docker push localhost:5000/myfirstimage

#Pull it back
docker pull localhost:5000/myfirstimage

#Now stop your registry and remove all data
docker container stop registry && docker container rm -v registry

```

