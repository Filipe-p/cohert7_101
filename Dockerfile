# We want redhat/centos machine 
# FROM is the base container that you'll build on
FROM centos:centos8.3.2011 
#pinning your version is good practice

# RUN is used to install at build time 
# We want httpd 
## this is where you need to know some base OS/BASH 
RUN  yum -y install httpd

# we want our own index file 
## COPY ./loca_dir /var/dir_container
COPY index.html /var/www/html

# PORT for orchestration tool
    # Meta data really important for Docker compose and kubernetes
    # Meta data is data about something that is not the something.
    # example, the data on this docker file is all these lines of code. But the META data is when was it create, by who, how much space does it take and so on.
    # therefore this is just a tag to tell other services we have installed or set something up that uses these ports.
    # this will allow other services to pick them up and orchestrate. 
EXPOSE 80
EXPOSE 443

## ENTRYPOINT
ENTRYPOINT ["httpd", "-DFOREGROUND"]
